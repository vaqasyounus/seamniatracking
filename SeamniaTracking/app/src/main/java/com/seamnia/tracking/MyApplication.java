package com.seamnia.tracking;

import android.app.Application;
import android.content.Context;
import android.graphics.Path;
import android.location.LocationManager;
import android.support.multidex.MultiDex;
import android.widget.Toast;

import com.akexorcist.googledirection.model.Direction;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;


public class MyApplication extends Application {
    public static LocationManager locationManager;
    public static List<LatLng> latLngList;
    public static Direction direction;


    @Override
    public void onCreate() {
        super.onCreate();
        //MultiDex.install(this);
        FirebaseApp.initializeApp(this.getApplicationContext());
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }

}
