package com.seamnia.tracking.models;

public class ConnectivityModel {

    boolean isLocationEnabled;

    public boolean isLocationEnabled() {
        return isLocationEnabled;
    }

    public void setLocationEnabled(boolean locationEnabled) {
        isLocationEnabled = locationEnabled;
    }
}
