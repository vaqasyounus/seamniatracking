package com.seamnia.tracking.Services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.PolyUtil;
import com.seamnia.tracking.MyApplication;
import com.seamnia.tracking.R;
import com.seamnia.tracking.activities.MapsActivity;
import com.seamnia.tracking.models.ConnectivityModel;
import com.seamnia.tracking.models.Constant;
import com.seamnia.tracking.models.LocationModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.seamnia.tracking.MyApplication.latLngList;
import static com.seamnia.tracking.MyApplication.locationManager;
import static com.seamnia.tracking.models.Constant.FOREGROUND_SERVICE_ID;
import static com.seamnia.tracking.models.Constant.NOTIFICATION_ID;

public class FusedLocationService extends Service {
    private static final String TAG = FusedLocationService.class.getSimpleName();

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location lastKnownLocation;
    public LocationRequest mLocationRequest;
    private LocationSettingsRequest.Builder builder;


    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;

    private PowerManager.WakeLock mWakelock;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference locationDatabaseReference;
    DatabaseReference onOffdatabaseReference;
    DatabaseReference gpsDatabaseReference;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        EventBus.getDefault().register(this);
        buildNotification();

        firebaseDatabase = FirebaseDatabase.getInstance();
        locationDatabaseReference = firebaseDatabase.getReference("CurrentLocation");
        onOffdatabaseReference = firebaseDatabase.getReference("isTracking");
        onOffdatabaseReference.setValue(true);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "paritalWakeLock");
        mWakelock.acquire();


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lastKnownLocation = location;
                }
            }
        });


        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constant.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(Constant.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setSmallestDisplacement(Constant.SMALLEST_DISPLACEMENT_IN_METER);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult location) {
                Location currentLocation = location.getLastLocation();
                if (currentLocation == null) {
                    currentLocation = lastKnownLocation;
                }

                LocationModel locationModel = new LocationModel();
                locationModel.setLatitude(currentLocation.getLatitude());
                locationModel.setLongitude(currentLocation.getLongitude());
                locationModel.setAccuracy(currentLocation.getAccuracy());

                if (MyApplication.direction!=null){

                }

                if (latLngList != null) {
                    if (PolyUtil.isLocationOnPath(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), latLngList, true, 100)) {
                        locationModel.setOnPath(true);
                    } else {
                        locationModel.setOnPath(false);
                    }
                }
                locationModel.setTime(currentLocation.getTime());
                locationModel.setSpeed(currentLocation.getSpeed() * 18 / 5);
                locationModel.setPower(getBatteryLevel());

                EventBus.getDefault().post(locationModel);
                locationDatabaseReference.setValue(locationModel);

            }

        };

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if (ActivityCompat.checkSelfPermission(FusedLocationService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(FusedLocationService.this, "Grant Location Permision", Toast.LENGTH_SHORT).show();
                    return;
                }
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);

            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });


    }

    private void buildNotification() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MapsActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.bus_white)
                .setColor(getColor(R.color.colorPrimary))
                .setContentTitle(getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setOngoing(true);
//                .setContentIntent(resultPendingIntent);
        startForeground(FOREGROUND_SERVICE_ID, mNotificationBuilder.build());
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mNotificationBuilder.setContentText("You're being tracked...");
        } else {
            mNotificationBuilder.setContentText("Turn on your GPS!");
        }
        mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
    }

    private float getBatteryLevel() {
        Intent batteryStatus = registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int batteryLevel = -1;
        int batteryScale = 1;
        if (batteryStatus != null) {
            batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, batteryLevel);
            batteryScale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, batteryScale);
        }
        return batteryLevel / (float) batteryScale * 100;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void gpsListner(ConnectivityModel connectivityModel) {
        if (!connectivityModel.isLocationEnabled())
            gpsDatabaseReference = firebaseDatabase.getReference("gps");
        gpsDatabaseReference.setValue(connectivityModel.isLocationEnabled());

    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        sendBroadcast(new Intent("YouWillNeverKillMe"));
        mNotificationManager.cancel(NOTIFICATION_ID);
        // Stop receiving location updates.
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        // Release the wakelock
        if (mWakelock != null) {
            mWakelock.release();
        }
        super.onDestroy();
    }
}