package com.seamnia.tracking.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.seamnia.tracking.Services.FusedLocationService;
import com.seamnia.tracking.Services.UtilityService;
import com.seamnia.tracking.activities.SplashActivity;

public class RestartServiceReceiver extends BroadcastReceiver
{

    private static final String TAG = "RestartServiceReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context.getApplicationContext(), UtilityService.class));
        context.startService(new Intent(context.getApplicationContext(), FusedLocationService.class));
    }
}