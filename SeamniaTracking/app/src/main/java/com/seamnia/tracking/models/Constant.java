package com.seamnia.tracking.models;

public class Constant {
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    public static final long SMALLEST_DISPLACEMENT_IN_METER = 1;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    public static final int NOTIFICATION_ID = 1;
    public static final int FOREGROUND_SERVICE_ID = 1;
    public static final int GPS_ENABLE_REQUEST = 0x1001;

}
