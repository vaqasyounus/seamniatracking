package com.seamnia.tracking.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

import com.seamnia.tracking.Services.FusedLocationService;
import com.seamnia.tracking.models.AppModel;
import com.seamnia.tracking.models.ConnectivityModel;

import org.greenrobot.eventbus.EventBus;

import static com.seamnia.tracking.MyApplication.locationManager;

public class LocationProviderReceiver extends BroadcastReceiver {
    ConnectivityModel connectivityModel;
//25.757701 ,68.653952
//25.760124 ,68.654522

    @Override
    public void onReceive(Context context, Intent intent) {
        connectivityModel = new ConnectivityModel();
        try {
            if (AppModel.getInstance().isServiceRunning(FusedLocationService.class, context)){
                context.stopService(new Intent(context.getApplicationContext(), FusedLocationService.class));
            }
            connectivityModel.setLocationEnabled(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
            EventBus.getDefault().post(connectivityModel);
            Toast.makeText(context, " not Exception", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Toast.makeText(context, "Exception", Toast.LENGTH_SHORT).show();
        }
    }
}