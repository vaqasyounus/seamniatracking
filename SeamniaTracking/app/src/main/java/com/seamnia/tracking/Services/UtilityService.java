package com.seamnia.tracking.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.seamnia.tracking.models.AppModel;

public class UtilityService extends Service {
    private static final String TAG = UtilityService.class.getSimpleName();


    private PowerManager.WakeLock mWakelock;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference onOffDatabaseReference;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "paritalWakeLock");
        mWakelock.acquire();

        firebaseDatabase = FirebaseDatabase.getInstance();
        onOffDatabaseReference = firebaseDatabase.getReference("isTracking");
        onOffDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Boolean value = dataSnapshot.getValue(Boolean.class);
                if (value) {
                    if (!AppModel.getInstance().isServiceRunning(FusedLocationService.class, UtilityService.this)) {
                        startService(new Intent(UtilityService.this, FusedLocationService.class));
                    }
                } else {
                    if (AppModel.getInstance().isServiceRunning(FusedLocationService.class, UtilityService.this)) {
                        stopService(new Intent(UtilityService.this, FusedLocationService.class));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }

        });


    }

    @Override
    public void onDestroy() {
        sendBroadcast(new Intent("YouWillNeverKillMe"));

        if (mWakelock != null) {
            mWakelock.release();
        }
        super.onDestroy();
    }
}