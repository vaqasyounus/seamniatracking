package com.seamnia.tracking.activities;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.seamnia.tracking.R;
import com.seamnia.tracking.Services.UtilityService;
import com.seamnia.tracking.Utilities.HelperUtilities;
import com.seamnia.tracking.models.AppModel;

import me.wangyuwei.particleview.ParticleView;

import static com.seamnia.tracking.MyApplication.locationManager;


public class SplashActivity extends AppCompatActivity {
    DatabaseReference metadatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (!AppModel.getInstance().isServiceRunning(UtilityService.class, SplashActivity.this)) {
            startService(new Intent(this, UtilityService.class));
        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        metadatabaseReference = FirebaseDatabase.getInstance().getReference("gps");
        metadatabaseReference.setValue(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));

        ParticleView particleView = (ParticleView) findViewById(R.id.pv);
        particleView.startAnim();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        particleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                if (HelperUtilities.getInstance().isGooglePlayServicesAvailable(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, MapsActivity.class));
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}