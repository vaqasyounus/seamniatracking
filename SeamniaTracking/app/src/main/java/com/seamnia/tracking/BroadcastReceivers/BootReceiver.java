package com.seamnia.tracking.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.seamnia.tracking.Services.FusedLocationService;
import com.seamnia.tracking.activities.SplashActivity;

public class BootReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = BootReceiver.class.toString();

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context,SplashActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.getApplicationContext().startActivity(i);
    }

}